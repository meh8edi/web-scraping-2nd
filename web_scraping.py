import os
import requests
import datetime
from bs4 import BeautifulSoup
from requests.api import head
import urllib3

urllib3.disable_warnings()
URL = "https://mofa.gov.bd/site/view/service_box_items/PRESS%20RELEASES/Press-Release"
r = requests.get(URL)


soup = BeautifulSoup(r.content, 'html5lib')
folder = 'Newfolder'
table = soup.find("table")

trs = table.find_all("tr")

ct = datetime.datetime.now()
ts = ct.timestamp()

url = URL.split("/")

name = str(url[2]) + "_" + str(ts) + "_" + "1711322"
with open(f"{name}.csv", "w", encoding="UTF-8") as f:

    for tr in trs:
        tds = tr.find_all("td")
        f.write(str(tds[0].contents[0]) + ",")
        f.write(str(tds[1].contents[1].contents[0])+",")
        f.write(str(tds[2].contents[0]) + "\n")

head = soup.find('head')
body = soup.find('body')

ct = datetime.datetime.now()
ts = ct.timestamp()

url = URL.split("/")

name = str(url[2]) + "_" + str(ts) + "_" + "1711322"
with open(f"{name}.txt", "w", encoding="UTF-8") as f:
    f.write(str(head))
    f.write(str(body))


try:
    os.mkdir(os.path.join(os.getcwd(), folder))
except:
    pass
os.chdir(os.path.join(os.getcwd(), folder))
i = 1
images = soup.find_all('img')
for image in images:
    link = image['src']
    if link.endswith('.jpeg'):
        if link.startswith("//"):
            link = "http:"+link
            with open(str(i) + ".jpeg", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)

        elif link.startswith("/"):

            link = "https://mofa.gov.bd"+link

            with open(str(i) + ".jpeg", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)

        else:
            with open(str(i) + ".jpeg", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)

    elif link.endswith('.png'):

        if link.startswith("//"):

            link = "http:"+link

            with open(str(i) + ".png", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)
        elif link.startswith("/"):

            link = "https://mofa.gov.bd"+link

            with open(str(i) + ".png", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)

        else:
            with open(str(i) + ".png", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)

    elif link.endswith('.bmp'):

        if link.startswith("//"):

            link = "http:"+link

            with open(str(i) + ".bmp", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)
        elif link.startswith("/"):

            link = "https://mofa.gov.bd"+link

            with open(str(i) + ".bmp", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)

        else:
            with open(str(i) + ".bmp", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)

    elif link.endswith('.jpg'):

        if link.startswith("//"):

            link = "http:"+link

            with open(str(i) + ".jpg", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)
        elif link.startswith("/"):

            link = "https://mofa.gov.bd"+link

            with open(str(i) + ".jpg", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)
        else:
            with open(str(i) + ".jpg", 'wb') as f:
                try:
                    im = requests.get(link)
                    f.write(im.content)
                    print('Done: ', i)
                except:
                    im = requests.get(link, verify=False)
                    f.write(im.content)
                    print('Done: ', i)

    i = i + 1